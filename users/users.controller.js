const express = require('express');
const router = express.Router();
const userService = require('./user.service');
const upload = require('_helpers/upload');

// routes
router.post('/login', authenticate);
router.post('/register', register);
router.get('/get-video', getVideosByUser);
router.get('/get-pro', getPro);
router.post('/upload', upload.single('avatar'), uploadFile)
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.put('/:id', upload.single('avatar'), update);
router.delete('/:id', _delete);


module.exports = router;


function uploadFile(req, res, next) {

	if (req.fileValidationError) {
            return res.send(req.fileValidationError);
        }
        else if (!req.file) {
            return res.send('Please select an image to upload');
        }

        // Display uploaded image for user validation
        res.send(`You have uploaded this image: <hr/><img src="http://192.168.1.108:4000/${req.file.path}" width="500"><hr /><a href="./">Upload another image</a>`);

}

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function register(req, res, next) {
//console.log('req.body=' + req.body);
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getPro(req, res, next) {
    userService.getPro()
        .then(pros => res.json(pros))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    if(req.file) req.body.photo = req.file.path;
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getVideosByUser(req, res, next) {
    userService.getVideosByUser(req.query.id)
        .then(users => res.json(users))
        .catch(err => next(err));
}
