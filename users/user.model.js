const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: { type: String, required: true },
    hash: { type: String, required: true },
    email: { type: String, uniquie: true, required: true },
    role: { type: String, default: "client", required: true },
    gender: {type: String},
    about: {type: String},
    photo: {type: String},
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);