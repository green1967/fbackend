const db = require('_helpers/db');
const Activity = db.Activity;

module.exports = {
    getAll,

};

async function getAll() {
    return await Activity.find();
}
