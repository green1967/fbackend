const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
                    _id: {type: String},
                    type_name: {type: String},
                    orders: {type: String},
                    description: {type: String},
                    ico_path: {type: String}		            
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Activity', schema);