const express = require('express');
const router = express.Router();
const activityService = require('./activity.service');

// routes
router.get('/', getAll);


module.exports = router;


function getAll(req, res, next) {
    activityService.getAll()
        .then(activities => res.json(activities))
        .catch(err => next(err));
}