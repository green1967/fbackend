const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
                    _id: {type: String},
                    music_genre: {type: String}
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Genre', schema);