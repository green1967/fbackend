const db = require('_helpers/db');
const Genre = db.Genre;

module.exports = {
    getAll,

};

async function getAll() {
    return await Genre.find();
}
