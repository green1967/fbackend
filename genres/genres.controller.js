const express = require('express');
const router = express.Router();
const genreService = require('./genre.service');

// routes
router.get('/', getAll);


module.exports = router;


function getAll(req, res, next) {
        genreService.getAll()
        .then(genres => res.json(genres))
        .catch(err => next(err));
}