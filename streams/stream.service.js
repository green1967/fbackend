const db = require('_helpers/db');
const Stream = db.Stream;
const Activity = db.Activity;

module.exports = {
    getGymStreamHome,
    getFeaturedStream,
	getAll
};

async function getGymStreamHome() {
    return await Activity.aggregate([
		{$lookup:
			{
			from: "streams", 
			let: {a_id: "$_id"}, 
			pipeline: [{$match: {$expr: {$eq: ["$activity_id", "$$a_id"]}}}, {$limit:6}], 
			as:"videos"
			}
		}]);
}

async function getFeaturedStream() {
    return await Stream.findOne({featured : 1});
}

async function getAll() {
    return await Stream.find();
}
