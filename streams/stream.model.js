const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
		            user_id: {type: String},
                    activity_id: {type: String},
                    title: {type: String},
                    file_name: {type: String},
                    image: {type: String},
                    is_live: {type: String},
                    viwers_cnt: {type: String},
                    is_viwe: {type: String},
                    plan_date: {type: String},
                    full_plan_date: {type: String},
                    duration : {type: String},
                    description: {type: String},
                    start_date: {type: String},
                    end_date: {type: String},
                    is_active : {type: String},
                    created_by: {type: String},
                    user_name: {type: String},
                    review_cnt: {type: String},
                    review_score: {type: String},
                    photo: {type: String},
                    activity_name: {type: String},
                    video_quality: {type: String},
                    stream_status: {type: String},
                    level_id: {type: String},
                    music_genre_id: {type: String},
		            featured: {type: String},
                    gym_level: {type: String},
                    admin_start_date: {type: String},
                    admin_end_date: {type: String}
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Stream', schema);