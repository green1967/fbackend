const express = require('express');
const router = express.Router();
const streamService = require('./stream.service');

// routes
router.get('/get-feature-stream', getFeaturedStream);
router.get('/get-gym-stream-home', getGymStreamHome);
router.get('/', getAll);


module.exports = router;

function getFeaturedStream(req, res, next) {
    streamService.getFeaturedStream()
        .then(stream => res.json(stream))
        .catch(err => next(err));
}

function getGymStreamHome(req, res, next) {
    streamService.getGymStreamHome()
        .then(streams => res.json(streams))
        .catch(err => next(err));
}

function getVideosByUser(req, res, next) {
    streamService.getVideosByUser()
	.then(streams => res.json(streams))
	.catch(err => next(err));
}

function getAll(req, res, next) {
    streamService.getAll()
    .then(streams => res.json(streams))
    .catch(err => next(err));
}


//function getCurrent(req, res, next) {
//    userService.getById(req.user.sub)
//        .then(user => user ? res.json(user) : res.sendStatus(404))
//        .catch(err => next(err));
//}

//function getById(req, res, next) {
//    userService.getById(req.params.id)
//        .then(user => user ? res.json(user) : res.sendStatus(404))
//        .catch(err => next(err));
//}
