const config = require('config.json');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
//mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model'),
    Stream: require('../streams/stream.model'),
    Activity: require('../activities/activitiy.model'),
    Equip: require('../equips/equip.model'),
    Level: require('../levels/level.model'),
    Genre: require('../genres/genre.model')
};

//let grasefulShutdown = function (msg, callback) {
//    mongoose.connection.close(function() {
//    console.log("Mongoose disconnected through " + msg);
//})
//}

//mongoose.connection.on('connected', function() {
//    console.log("Mongoose connected");
//})

//mongoose.connection.on('error', function(err) {
//    console.log("Mongoose connection error:" + err);
//})

//mongoose.connection.on('disconnect', function() {
//    console.log("Mongoose disconnected");
//})

//process.on('SIGINT', function() {
//    grasefulShutdown('app termination', function() {
//    process.exit();
//    process.exitCode = 1;
//})
//})

//process.on('SIGUSR2', function() {
//    grasefulShutdown('nodemon restart', function() {
//    process.kill(process.pid, 'SIGUSR2');
//})
//})


