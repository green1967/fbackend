const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
                    _id: {type: String},
                    name: {type: String},
                    is_active: {type: String}		            
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Equip', schema);