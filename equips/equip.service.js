const db = require('_helpers/db');
const Equip = db.Equip;

module.exports = {
    getAll,

};

async function getAll() {
    return await Equip.find();
}
