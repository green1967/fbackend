const express = require('express');
const router = express.Router();
const equipService = require('./equip.service');

// routes
router.get('/', getAll);


module.exports = router;


function getAll(req, res, next) {
        equipService.getAll()
        .then(equips => res.json(equips))
        .catch(err => next(err));
}