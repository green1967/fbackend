const express = require('express');
const router = express.Router();
const levelService = require('./level.service');

// routes
router.get('/', getAll);


module.exports = router;


function getAll(req, res, next) {
        levelService.getAll()
        .then(levels => res.json(levels))
        .catch(err => next(err));
}