const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
                    _id: {type: String},
                    name: {type: String},
                    orders: {type: String}		            
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Level', schema);