const db = require('_helpers/db');
const Level = db.Level;

module.exports = {
    getAll,

};

async function getAll() {
    return await Level.find();
}
