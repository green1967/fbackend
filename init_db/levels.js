db.levels.insertMany([
{
                    "_id": "1",
                    "name": "Beginner",
                    "orders": "1"
                },
                {
                    "_id": "2",
                    "name": "Intermediate",
                    "orders": "2"
                },
                {
                    "_id": "3",
                    "name": "Advanced",
                    "orders": "3"
                },
                {
                    "_id": "4",
                    "name": "Low Impact",
                    "orders": "4"
                }
])