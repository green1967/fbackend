db.activities.insertMany([
    {
        "_id": "3",
        "type_name": "Cycling",
        "orders": "1",
        "description": "",
        "ico_path": "/img/03.png",
        "equipment": [
            {
                "activity_id": "3",
                "equip_id": "4",
                "equip_name": "Cycle",
                "is_auto": "1",
                "orders": "1",
                "is_active": "0"
            },
            {
                "activity_id": "3",
                "equip_id": "5",
                "equip_name": "Dumbbells",
                "is_auto": "0",
                "orders": "2",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "5",
        "type_name": "Elliptical",
        "orders": "2",
        "description": "",
        "ico_path": "/img/05.png",
        "equipment": [
            {
                "activity_id": "5",
                "equip_id": "10",
                "equip_name": "Mat",
                "is_auto": "1",
                "orders": "0",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "4",
        "type_name": "Treadmill",
        "orders": "3",
        "description": "",
        "ico_path": "/img/04.png",
        "equipment": [
            {
                "activity_id": "4",
                "equip_id": "13",
                "equip_name": "Treadmill",
                "is_auto": "1",
                "orders": "1",
                "is_active": "0"
            },
            {
                "activity_id": "4",
                "equip_id": "5",
                "equip_name": "Dumbbells",
                "is_auto": "0",
                "orders": "2",
                "is_active": "0"
            },
            {
                "activity_id": "4",
                "equip_id": "11",
                "equip_name": "Resistance bands",
                "is_auto": "0",
                "orders": "2",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "6",
        "type_name": "Rowing",
        "orders": "4",
        "description": "",
        "ico_path": "/img/06.png",
        "equipment": [
            {
                "activity_id": "6",
                "equip_id": "10",
                "equip_name": "Mat",
                "is_auto": "1",
                "orders": "0",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "12",
        "type_name": "AirBike",
        "orders": "5",
        "description": "",
        "ico_path": "/img/10.png",
        "equipment": [
            {
                "activity_id": "12",
                "equip_id": "5",
                "equip_name": "Dumbbells",
                "is_auto": "0",
                "orders": "1",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "8",
        "type_name": "Core",
        "orders": "6",
        "description": "",
        "ico_path": "/img/08.png",
        "equipment": [
            {
                "activity_id": "8",
                "equip_id": "11",
                "equip_name": "Resistance bands",
                "is_auto": "0",
                "orders": "1",
                "is_active": "0"
            },
            {
                "activity_id": "8",
                "equip_id": "9",
                "equip_name": "Magic circle",
                "is_auto": "0",
                "orders": "2",
                "is_active": "0"
            },
            {
                "activity_id": "8",
                "equip_id": "7",
                "equip_name": "Foam roller",
                "is_auto": "0",
                "orders": "3",
                "is_active": "0"
            },
            {
                "activity_id": "8",
                "equip_id": "1",
                "equip_name": "Ball",
                "is_auto": "0",
                "orders": "4",
                "is_active": "0"
            },
            {
                "activity_id": "8",
                "equip_id": "5",
                "equip_name": "Dumbbells",
                "is_auto": "0",
                "orders": "5",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "9",
        "type_name": "Yoga",
        "orders": "7",
        "description": "",
        "ico_path": "/img/02.png",
        "equipment": [
            {
                "activity_id": "9",
                "equip_id": "10",
                "equip_name": "Mat",
                "is_auto": "1",
                "orders": "1",
                "is_active": "0"
            },
            {
                "activity_id": "9",
                "equip_id": "14",
                "equip_name": "Yoga blocks",
                "is_auto": "0",
                "orders": "2",
                "is_active": "0"
            },
            {
                "activity_id": "9",
                "equip_id": "15",
                "equip_name": "Yoga strap",
                "is_auto": "0",
                "orders": "3",
                "is_active": "0"
            },
            {
                "activity_id": "9",
                "equip_id": "17",
                "equip_name": "Blanket",
                "is_auto": "0",
                "orders": "4",
                "is_active": "0"
            },
            {
                "activity_id": "9",
                "equip_id": "3",
                "equip_name": "Bolster",
                "is_auto": "0",
                "orders": "5",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "10",
        "type_name": "Tai Chi",
        "orders": "8",
        "description": "",
        "ico_path": "/img/07.png",
        "equipment": [
            {
                "activity_id": "10",
                "equip_id": "10",
                "equip_name": "Mat",
                "is_auto": "1",
                "orders": "0",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "11",
        "type_name": "Pilates",
        "orders": "9",
        "description": "",
        "ico_path": "/img/09.png",
        "equipment": [
            {
                "activity_id": "11",
                "equip_id": "1",
                "equip_name": "Ball",
                "is_auto": "0",
                "orders": "1",
                "is_active": "0"
            },
            {
                "activity_id": "11",
                "equip_id": "11",
                "equip_name": "Resistance bands",
                "is_auto": "0",
                "orders": "2",
                "is_active": "0"
            },
            {
                "activity_id": "11",
                "equip_id": "9",
                "equip_name": "Magic circle",
                "is_auto": "0",
                "orders": "3",
                "is_active": "0"
            },
            {
                "activity_id": "11",
                "equip_id": "5",
                "equip_name": "Dumbbells",
                "is_auto": "0",
                "orders": "4",
                "is_active": "0"
            },
            {
                "activity_id": "11",
                "equip_id": "7",
                "equip_name": "Foam roller",
                "is_auto": "0",
                "orders": "5",
                "is_active": "0"
            }
        ]
    },
    {
        "_id": "1",
        "type_name": "Strength",
        "orders": "10",
        "description": "",
        "ico_path": "/img/01.png",
        "equipment": [
            {
                "activity_id": "1",
                "equip_id": "2",
                "equip_name": "Bench",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "5",
                "equip_name": "Dumbbells",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "6",
                "equip_name": "Exercise ball",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "7",
                "equip_name": "Foam roller",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "8",
                "equip_name": "Kettlebells",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "10",
                "equip_name": "Mat",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "11",
                "equip_name": "Resistance bands",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "12",
                "equip_name": "Slam ball",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            },
            {
                "activity_id": "1",
                "equip_id": "18",
                "equip_name": "No Equipment",
                "is_auto": "0",
                "orders": "0",
                "is_active": "0"
            }
        ]
    }
])