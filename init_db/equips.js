db.equips.insertMany([
    {
        "_id": "1",
        "name": "Ball",
        "is_active": ""
    },
    {
        "_id": "2",
        "name": "Bench",
        "is_active": ""
    },
    {
        "_id": "17",
        "name": "Blanket",
        "is_active": ""
    },
    {
        "_id": "3",
        "name": "Bolster",
        "is_active": ""
    },
    {
        "_id": "4",
        "name": "Cycle",
        "is_active": ""
    },
    {
        "_id": "5",
        "name": "Dumbbells",
        "is_active": ""
    },
    {
        "_id": "6",
        "name": "Exercise ball",
        "is_active": ""
    },
    {
        "_id": "7",
        "name": "Foam roller",
        "is_active": ""
    },
    {
        "_id": "8",
        "name": "Kettlebells",
        "is_active": ""
    },
    {
        "_id": "9",
        "name": "Magic circle",
        "is_active": ""
    },
    {
        "_id": "10",
        "name": "Mat",
        "is_active": ""
    },
    {
        "_id": "18",
        "name": "No Equipment",
        "is_active": ""
    },
    {
        "_id": "11",
        "name": "Resistance bands",
        "is_active": ""
    },
    {
        "_id": "12",
        "name": "Slam ball",
        "is_active": ""
    },
    {
        "_id": "13",
        "name": "Treadmill",
        "is_active": ""
    },
    {
        "_id": "14",
        "name": "Yoga blocks",
        "is_active": ""
    },
    {
        "_id": "15",
        "name": "Yoga strap",
        "is_active": ""
    }
])