db.genres.insertMany([
    {
        "_id": "15",
        "music_genre": "60s"
    },
    {
        "_id": "16",
        "music_genre": "70s"
    },
    {
        "_id": "17",
        "music_genre": "80s"
    },
    {
        "_id": "1",
        "music_genre": "Alternative"
    },
    {
        "_id": "14",
        "music_genre": "Broadway"
    },
    {
        "_id": "12",
        "music_genre": "Chillax"
    },
    {
        "_id": "2",
        "music_genre": "Classic Rock"
    },
    {
        "_id": "3",
        "music_genre": "Country"
    },
    {
        "_id": "11",
        "music_genre": "Eclectic Blends"
    },
    {
        "_id": "4",
        "music_genre": "Electronic"
    },
    {
        "_id": "5",
        "music_genre": "Hip Hop"
    },
    {
        "_id": "6",
        "music_genre": "Indie"
    },
    {
        "_id": "7",
        "music_genre": "Latin"
    },
    {
        "_id": "8",
        "music_genre": "Pop"
    },
    {
        "_id": "9",
        "music_genre": "R&B"
    },
    {
        "_id": "10",
        "music_genre": "Rock"
    },
    {
        "_id": "13",
        "music_genre": "World"
    }
])