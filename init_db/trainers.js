db.users.insertMany([
    {
        "_id": "1994",
        "pid": "1994",
        "name": "Nika Eshetu",
        "email":"nika@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "2",
            "1",
            "8"
        ],
        "activity_names": "Strength, Core",
        "photo": "/public/photo/avatar_abbate.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "13",
        "role": "pro"
    },
    {
        "_id": "3029",
        "pid": "3029",
        "name": "Brinn Abbate",
        "email":"brinn@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "3",
            "5",
            "4"
        ],
        "activity_names": "Cycling, Treadmill, Elliptical",
        "photo": "/public/photo/avatar_brinn.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "18",
        "role": "pro"
    },
    {
        "_id" : "4463",
        "pid" : "4463",
        "name": "Salmannsa Rodgers",
        "email" : "salmansa@trainers.com",
        "about" : "",
        "degree_level" : "",
        "role" : "pro",
        "state": "CA",
        "city_name" : "Los Angeles",
        "activity_id" : [
            "4"
        ],
        "activity_names" : "Treadmill",
        "photo": "/public/photo/avatar_salmannsa.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "4"
    },
    {
        "_id": "4450",
        "pid": "4450",
        "name": "Meghan Grim",
        "email":"meghan@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "4",
            "5",
            "3"
        ],
        "activity_names": "Cycling, Treadmill, Elliptical",
        "photo": "/public/photo/avatar_grim.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "6"
    },
    {
        "_id": "4446",
        "pid": "4446",
        "name": "Cherie Bodenstab",
        "email":"cherie@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "7"
        ],
        "activity_names": null,
        "photo": "/public/photo/avatar_abbate.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "3",
        "role": "pro"
    },
    {
        "_id": "4444",
        "pid": "4444",
        "name": "Bailey Jung",
        "email":"bailey@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "6"
        ],
        "activity_names": "Rowing",
        "photo": "/public/photo/avatar_bailey.jpg",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "6",
        "role": "pro"
    },
    {
        "_id": "4394",
        "pid": "4394",
        "name": "David Pisanich",
        "email":"david@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "3",
            "4",
            "8"
        ],
        "activity_names": "Cycling, Treadmill, Core",
        "photo": "/public/photo/avatar_pisanich.png",
        "is_follower": "1",
        "is_live": "0",
        "count_followers": "12",
        "role": "pro"
    },
    {
        "_id": "4354",
        "pid": "4354",
        "name": "Julia Baggish",
        "email":"julia@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "3",
            "5"
        ],
        "activity_names": "Cycling, Elliptical",
        "photo": "/public/photo/avatar_baggish.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "9",
        "role": "pro"
    },
    {
        "_id": "3784",
        "pid": "3784",
        "name": "Vincent Bowe",
        "email":"vincent@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": " CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "1",
            "8"
        ],
        "activity_names": "Strength, Core",
        "photo": "/public/photo/avatar_pisanich.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "10",
        "role": "pro"
    },
    {
        "_id": "3031",
        "pid": "3031",
        "name": "Alessandra Bonetti",
        "email":"alesandra@trainers.com",
        "about" : "",
        "degree_level" : "",
        "state": "CA",
        "city_name": "Los Angeles",
        "activity_id": [
            "2",
            "8"
        ],
        "activity_names": "Core",
        "photo": "/public/photo/avatar_abbate.png",
        "is_follower": "0",
        "is_live": "0",
        "count_followers": "9",
        "role": "pro"
    }
])